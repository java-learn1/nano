package com.udacity.testingdog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestingdogApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestingdogApplication.class, args);
	}

}
