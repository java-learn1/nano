package com.udacity.documentingdog.service;

import com.udacity.documentingdog.model.Dog;

import java.util.List;

public interface DogService {
    public List<Dog> retrieveDog();

    List<String> retrieveDogBreed();

    String retrieveDogBreedById(Long id);

    List<String> retrieveDogNames();
}
