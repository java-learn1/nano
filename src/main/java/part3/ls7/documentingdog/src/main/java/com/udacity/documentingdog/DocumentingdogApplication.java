package com.udacity.documentingdog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocumentingdogApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocumentingdogApplication.class, args);
	}

}
