package com.udacity.securitydog.service;

import com.udacity.securitydog.model.Dog;
import com.udacity.securitydog.repository.DogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DogServiceImp implements DogService {
    @Autowired
    DogRepository dogRepository;

    public List<Dog> retrieveDog(){
        return (List<Dog>)dogRepository.findAll();
    }

    public List<String> retrieveDogBreed(){
        return (List<String>)dogRepository.findAllBreed();
    }

    public List<String> retrieveDogNames(){
        return (List<String>) dogRepository.findAllName();
    }
}
