package com.udacity.securitydog.service;

import com.udacity.securitydog.model.Dog;
import java.util.List;

public interface DogService {
    public List<Dog> retrieveDog();

    List<String> retrieveDogBreed();

    List<String> retrieveDogNames();
}
