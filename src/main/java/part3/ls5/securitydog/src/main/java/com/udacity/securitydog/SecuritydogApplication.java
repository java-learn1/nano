package com.udacity.securitydog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecuritydogApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecuritydogApplication.class, args);
	}

}
