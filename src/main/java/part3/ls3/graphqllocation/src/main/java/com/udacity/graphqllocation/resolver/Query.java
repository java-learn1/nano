package com.udacity.graphqllocation.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.udacity.graphqllocation.entity.Location;
import com.udacity.graphqllocation.repository.LocationRepository;
import org.springframework.stereotype.Component;

@Component
public class Query implements GraphQLQueryResolver {
    private LocationRepository locationRepository;

    public Query(LocationRepository locationRepository){
        this.locationRepository = locationRepository;
    }

    public Iterable<Location> findAllLocations() {
        return locationRepository.findAll();
    }
}
