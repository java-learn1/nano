package com.udacity.graphqllocation.service;

import com.udacity.graphqllocation.entity.Location;
import com.udacity.graphqllocation.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LocationServiceImpl implements LocationService {
    @Autowired
    LocationRepository locationRepository;

    public List<Location> retrieveLocations() {
        return (List<Location>) locationRepository.findAll();
    }

    @Override
    public List<String> retrieveLocationNames() {
        return (List<String>) locationRepository.findAllNames();
    }

    @Override
    public String retrieveLocationNameById(Long id) {
        Optional<String> optionalBreed = Optional.ofNullable(locationRepository.findNameById(id));
        String name = optionalBreed.orElseThrow(LocationNotFoundException::new);
        return name;
    }

    @Override
    public List<String> retrieveLocationAddress() {
        return (List<String>) locationRepository.findAllAddress();
    }
}

