package com.udacity.graphqllocation.web;

import com.udacity.graphqllocation.entity.Location;
import com.udacity.graphqllocation.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LocationController {
    private LocationService locationService;

    @Autowired
    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping("/location")
    public ResponseEntity<List<Location>> getAllLocations() {
        List<Location> list = locationService.retrieveLocations();
        return new ResponseEntity<List<Location>>(list, HttpStatus.OK);
    }

    @GetMapping("/location/name")
    public ResponseEntity<List<String>> getDogBreed(){
        List<String> list = locationService.retrieveLocationNames();
        return new ResponseEntity<List<String>>(list,HttpStatus.OK);
    }

    @GetMapping("/{id}/name")
    public ResponseEntity<String> getDogBreedById(@PathVariable Long id){
        String name = locationService.retrieveLocationNameById(id);
        return new ResponseEntity<String>(name,HttpStatus.OK);
    }

    @GetMapping("/location/adrress")
    public ResponseEntity<List<String>> getDogNames(){
        List<String> list = locationService.retrieveLocationAddress();
        return new ResponseEntity<List<String>>(list,HttpStatus.OK);
    }
}

