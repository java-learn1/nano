package com.udacity.graphqllocation.repository;

import com.udacity.graphqllocation.entity.Location;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LocationRepository extends CrudRepository<Location, Long> {
    @Query("select l.id, l.name from Location l where l.id =:id")
    String findNameById(Long id);

    @Query("select l.id, l.name from Location l")
    List<String> findAllNames();

    @Query("select l.id, l.address from Location l")
    List<String> findAllAddress();

}

