package com.udacity.graphqllocation.service;

import com.udacity.graphqllocation.entity.Location;

import java.util.List;

public interface LocationService {
    List<Location> retrieveLocations();

    List<String> retrieveLocationNames();

    String retrieveLocationNameById(Long id);

    List<String> retrieveLocationAddress();

}

