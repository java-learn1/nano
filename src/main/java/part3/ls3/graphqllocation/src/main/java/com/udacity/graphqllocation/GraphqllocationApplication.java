package com.udacity.graphqllocation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphqllocationApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphqllocationApplication.class, args);
	}

}
