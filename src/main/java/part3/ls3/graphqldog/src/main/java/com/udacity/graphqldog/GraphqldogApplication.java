package com.udacity.graphqldog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphqldogApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphqldogApplication.class, args);
	}

}
