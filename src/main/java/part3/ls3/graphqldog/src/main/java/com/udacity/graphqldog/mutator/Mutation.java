package com.udacity.graphqldog.mutator;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.udacity.graphqldog.model.Dog;
import com.udacity.graphqldog.repository.DogRepository;
import com.udacity.graphqldog.service.BreedNotFoundException;
import com.udacity.graphqldog.service.DogNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class Mutation implements GraphQLMutationResolver {
    DogRepository dogRepository;

    public Mutation(DogRepository dogRepository) {
        this.dogRepository = dogRepository;
    }

    public Dog newDog(String name, String breed, String origin){
        Dog dog = new Dog(name, breed, origin);
        dogRepository.save(dog);
        return dog;
    }

    public boolean deleteDogBreed(String breed){
        boolean deleted = false;
        Iterable<Dog> allDogs = dogRepository.findAll();

        for (Dog dog:allDogs) {
            if(dog.getBreed().equals(breed)){
                dogRepository.delete(dog);
                deleted = true;
            }
        }
        // Throw an exception if the breed doesn't exist
        if (!deleted) {
            throw new BreedNotFoundException("Breed Not Found", breed);
        }
        return deleted;
    }

    public Dog updateDogName(String newName, Long id){
        Optional<Dog> optionalDog = dogRepository.findById(id);

        if (optionalDog.isPresent()){
            Dog dog = optionalDog.get();
            dog.setName(newName);

            dogRepository.save(dog);
            return dog;
        }else{
            throw new DogNotFoundException("Dog Not Found", id);
        }
    }
}
