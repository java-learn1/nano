package com.udacity.graphqldog.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.udacity.graphqldog.model.Dog;
import com.udacity.graphqldog.repository.DogRepository;
import com.udacity.graphqldog.service.DogNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class Query implements GraphQLQueryResolver {
    DogRepository dogRepository;

    public Query(DogRepository dogRepository) {
        this.dogRepository = dogRepository;
    }

    public Iterable<Dog> findAllDogs() {
        return dogRepository.findAll();
    }

    public Dog findDogById(Long id){
        Optional<Dog> optionalDog = dogRepository.findById(id);

        if (optionalDog.isPresent()){
              return optionalDog.get();
        }else{
            throw new DogNotFoundException("Dog Not Found", id);
        }
    }

    public String findDogBreedById (Long id){
        return dogRepository.findBreedById(id);
    }

    public List<String> findDogBreeds(){
        return dogRepository.findAllBreed();
    }



    public List<String> findAllDogNames (){
        return dogRepository.findAllName();
    }

}
