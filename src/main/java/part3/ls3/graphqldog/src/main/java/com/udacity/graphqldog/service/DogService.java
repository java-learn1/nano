package com.udacity.graphqldog.service;

import com.udacity.graphqldog.model.Dog;

import java.util.List;

public interface DogService {
    public List<Dog> retrieveDog();

    List<String> retrieveDogBreed();

    String retrieveDogBreedById(Long id);

    List<String> retrieveDogNames();
}
