package com.example.gusonano.spbvalidation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpbvalidationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpbvalidationApplication.class, args);
	}

}
