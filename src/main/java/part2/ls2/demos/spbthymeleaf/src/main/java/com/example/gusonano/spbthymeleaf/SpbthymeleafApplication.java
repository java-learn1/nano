package com.example.gusonano.spbthymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpbthymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpbthymeleafApplication.class, args);
	}

}
