package com.example.gusonano.spbwebsocketdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpbwebsocketdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpbwebsocketdemoApplication.class, args);
	}

}
