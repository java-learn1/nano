package com.example.gusonano.spbwebsocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpbwebsocketApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpbwebsocketApplication.class, args);
	}

}
