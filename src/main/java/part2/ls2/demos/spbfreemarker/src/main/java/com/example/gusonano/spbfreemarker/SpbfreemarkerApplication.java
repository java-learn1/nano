package com.example.gusonano.spbfreemarker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpbfreemarkerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpbfreemarkerApplication.class, args);
	}

}
