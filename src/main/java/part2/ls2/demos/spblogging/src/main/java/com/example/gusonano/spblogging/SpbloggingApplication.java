package com.example.gusonano.spblogging;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpbloggingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpbloggingApplication.class, args);
	}

}
