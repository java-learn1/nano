package com.example.gusonano.spbhelloworld;

import com.example.gusonano.spbhelloworld.controller.HelloController;
import com.example.gusonano.spbhelloworld.servlet.HelloFilter;
import com.example.gusonano.spbhelloworld.servlet.HelloListener;
import com.example.gusonano.spbhelloworld.servlet.HelloServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

import javax.servlet.ServletRegistration;

@SpringBootApplication
public class SpbhelloworldApplication {

	static final String URL = "/helloServlet";

	public static void main(String[] args) {
		SpringApplication.run(SpbhelloworldApplication.class, args);
	}

	//Registration Servlet
	@Bean
	public ServletRegistrationBean getServletRegistrationBean(){
		ServletRegistrationBean servletBean = new ServletRegistrationBean(new HelloServlet());
		servletBean.addUrlMappings(URL);
		return servletBean;
	}

	@Bean
	public FilterRegistrationBean getFilterRegistrationBean(){
		FilterRegistrationBean filterBean = new FilterRegistrationBean(new HelloFilter());
		//Add filter path
		filterBean.addUrlPatterns(URL);
		return filterBean;
	}

	@Bean
	public ServletListenerRegistrationBean<HelloListener> getServletListenerRegistrationBean(){
		ServletListenerRegistrationBean<HelloListener> listenerBean =
				new ServletListenerRegistrationBean<>(new HelloListener());
		return listenerBean;
	}

}
