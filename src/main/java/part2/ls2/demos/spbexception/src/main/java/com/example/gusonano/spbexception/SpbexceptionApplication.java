package com.example.gusonano.spbexception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpbexceptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpbexceptionApplication.class, args);
	}

}
