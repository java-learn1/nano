package com.example.gusonano.spbunittests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpbunittestsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpbunittestsApplication.class, args);
	}

}
