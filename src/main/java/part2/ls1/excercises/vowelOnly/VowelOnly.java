package part2.ls1.excercises.vowelOnly;

public class VowelOnly {

    public static String vowelOnly(String input) {
        String vowel = "aeiou";
        StringBuilder stringBuilder = new StringBuilder();
        for (char c: input.toCharArray()) {
            if(vowel.contains(String.valueOf(c).toLowerCase())){
                stringBuilder.append(c);
            }
        }
        return stringBuilder.toString();
    }
}
