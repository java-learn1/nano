package part2.ls1.excercises.calculator;

import org.junit.Assert;

public class CalculatorTest {

    @org.junit.Test
    public void calculate() throws Exception {
        String s = "1+2";
        Assert.assertEquals(Calculator.calculate(s), 3);
    }

    @org.junit.Test
    public void calculateTest2() throws Exception {
        Assert.assertEquals(Calculator.calculate("1+2*5"), 11);
    }
}
