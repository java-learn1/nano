package part2.ls1.excercises.reverseString;

import org.junit.Assert;
import org.junit.Test;

public class ReverseStringTest {

    @Test
    public void reverseStringTest(){
        Assert.assertEquals(ReverseString.reverseString("Hello World!"), "!dlroW olleH");
    }
}
