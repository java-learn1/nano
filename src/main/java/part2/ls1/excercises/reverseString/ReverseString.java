package part2.ls1.excercises.reverseString;

public class ReverseString {

    public static String reverseString(String input) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = input.length()-1; i >= 0 ; i--) {
            stringBuilder.append(input.charAt(i));
        }
        return stringBuilder.toString();
    }
}
