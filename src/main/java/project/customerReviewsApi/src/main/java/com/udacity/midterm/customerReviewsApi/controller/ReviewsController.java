package com.udacity.midterm.customerReviewsApi.controller;

import com.udacity.midterm.customerReviewsApi.entity.nosql.ReviewDocument;
import com.udacity.midterm.customerReviewsApi.entity.sql.Product;
import com.udacity.midterm.customerReviewsApi.entity.sql.Review;
import com.udacity.midterm.customerReviewsApi.repository.ProductRepository;
import com.udacity.midterm.customerReviewsApi.repository.ReviewDocumentRepository;
import com.udacity.midterm.customerReviewsApi.repository.ReviewRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Spring REST controller for working with review entity.
 */
@RestController
public class ReviewsController {

    private ReviewRepository reviewRepository;
    private ProductRepository productRepository;
    private ReviewDocumentRepository reviewDocumentRepository;

    public ReviewsController(ReviewRepository reviewsRepository, ProductRepository productsRepository, ReviewDocumentRepository reviewDocumentRepository) {
        this.reviewRepository = reviewRepository;
        this.productRepository = productRepository;
        this.reviewDocumentRepository = reviewDocumentRepository;
    }

    /**
     * Creates a review for a product.
     * <p>
     * 1. Add argument for review entity. Use {@link RequestBody} annotation.
     * 2. Check for existence of product.
     * 3. If product not found, return NOT_FOUND.
     * 4. If found, save review.
     *
     * @param productId The id of the product.
     * @return The created review or 404 if product id is not found.
     */
    @RequestMapping(value = "/reviews/products/{productId}", method = RequestMethod.POST)
    public ResponseEntity<Review> createReviewForProduct(@PathVariable("productId") Integer productId, @Valid @RequestBody Review review) {
        Optional<Product> product = productRepository.findById(productId);

        if (product.isPresent()) {
            review.setProduct(product.get());
            review = reviewRepository.save(review);

            ReviewDocument reviewDocument = new ReviewDocument();
            reviewDocument.setId(review.getId());
            reviewDocument.setProductId(review.getProduct().getId());
            reviewDocument.setReview(review.getReviewText());
            reviewDocument.setTitle(review.getTitle());

            reviewDocumentRepository.save(reviewDocument);

            return new ResponseEntity<>(review, HttpStatus.CREATED);
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Lists reviews by product.
     *
     * @param productId The id of the product.
     * @return The list of reviews.
     */
    /**
     * @param productId The id of the product.
     * @return The list of reviews.
     */
    @RequestMapping(value = "/reviews/products/{productId}", method = RequestMethod.GET)
    public ResponseEntity<List<ReviewDocument>> listReviewsForProduct(@PathVariable("productId") Integer productId) {
        List<ReviewDocument> reviews = reviewDocumentRepository.findAllByProductId(productId);
        if (reviews != null){
            return new ResponseEntity<>(reviews, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}