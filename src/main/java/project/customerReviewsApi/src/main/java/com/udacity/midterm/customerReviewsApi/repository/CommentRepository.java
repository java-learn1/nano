package com.udacity.midterm.customerReviewsApi.repository;

import com.udacity.midterm.customerReviewsApi.entity.sql.Comment;
import com.udacity.midterm.customerReviewsApi.entity.sql.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

    List<Comment> findAllByReview(Review review);
}
