package com.udacity.midterm.customerReviewsApi.repository;

import com.udacity.midterm.customerReviewsApi.entity.sql.Product;
import com.udacity.midterm.customerReviewsApi.entity.sql.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Integer> {
    List<Review> findAllByProduct(Product product);
}
