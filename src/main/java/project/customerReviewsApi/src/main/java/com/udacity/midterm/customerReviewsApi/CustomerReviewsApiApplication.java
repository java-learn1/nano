package com.udacity.midterm.customerReviewsApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerReviewsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerReviewsApiApplication.class, args);
	}

}
