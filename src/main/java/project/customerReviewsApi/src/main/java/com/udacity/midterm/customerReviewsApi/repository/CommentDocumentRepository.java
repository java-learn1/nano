package com.udacity.midterm.customerReviewsApi.repository;

import com.udacity.midterm.customerReviewsApi.entity.nosql.CommentDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentDocumentRepository extends MongoRepository<CommentDocument, String> {
    List<CommentDocument> findAllByReviewId(Integer reviewId);
}
