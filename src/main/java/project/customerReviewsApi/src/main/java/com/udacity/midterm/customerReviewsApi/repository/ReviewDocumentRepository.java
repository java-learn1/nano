package com.udacity.midterm.customerReviewsApi.repository;

import com.udacity.midterm.customerReviewsApi.entity.nosql.ReviewDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewDocumentRepository extends MongoRepository<ReviewDocument, String> {
    List<ReviewDocument> findAllByProductId(Integer productId);
}
