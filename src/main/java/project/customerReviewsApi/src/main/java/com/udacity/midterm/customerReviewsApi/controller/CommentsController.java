package com.udacity.midterm.customerReviewsApi.controller;

import com.udacity.midterm.customerReviewsApi.entity.nosql.CommentDocument;
import com.udacity.midterm.customerReviewsApi.entity.sql.Comment;
import com.udacity.midterm.customerReviewsApi.entity.sql.Review;
import com.udacity.midterm.customerReviewsApi.repository.CommentDocumentRepository;
import com.udacity.midterm.customerReviewsApi.repository.CommentRepository;
import com.udacity.midterm.customerReviewsApi.repository.ReviewDocumentRepository;
import com.udacity.midterm.customerReviewsApi.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Spring REST controller for working with comment entity.
 */
@RestController
@RequestMapping("/comments")
public class CommentsController {

    private CommentRepository commentRepository;
    private ReviewRepository reviewRepository;
    private CommentDocumentRepository commentDocumentRepository;
    private ReviewDocumentRepository reviewDocumentRepository;

    public CommentsController(CommentRepository commentRepository, ReviewRepository reviewRepository,
                              CommentDocumentRepository commentDocumentRepository, ReviewDocumentRepository reviewDocumentRepository){
        this.commentRepository = commentRepository;
        this.reviewRepository = reviewRepository;
        this.commentDocumentRepository = commentDocumentRepository;
        this.reviewDocumentRepository =  reviewDocumentRepository;
    }

    /**
     * @param reviewId The id of the review.
     */
    @RequestMapping(value = "/reviews/{reviewId}", method = RequestMethod.POST)
    public ResponseEntity<Comment> createCommentForReview(@PathVariable("reviewId") Integer reviewId, @Valid @RequestBody Comment comment) {
        Optional<Review> review = reviewRepository.findById(reviewId);

        if(review.isPresent()){
            comment.setReview(review.get());
            comment = commentRepository.save(comment);

            CommentDocument commentDocument = new CommentDocument();
            commentDocument.setId(comment.getId());
            commentDocument.setComment(comment.getCommentText());
            commentDocument.setReviewId(comment.getReview().getId());

            commentDocumentRepository.save(commentDocument);
            return new ResponseEntity<>(comment, HttpStatus.CREATED);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * @param reviewId The id of the review.
     */
    @RequestMapping(value = "/reviews/{reviewId}", method = RequestMethod.GET)
    public ResponseEntity<List<CommentDocument>> listCommentsForReview(@PathVariable("reviewId") Integer reviewId) {
        List<CommentDocument> reviewsList = commentDocumentRepository.findAllByReviewId(reviewId);
        if (reviewsList != null) {
            return new ResponseEntity<>(reviewsList, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}