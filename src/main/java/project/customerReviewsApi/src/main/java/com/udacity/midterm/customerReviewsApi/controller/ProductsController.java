package com.udacity.midterm.customerReviewsApi.controller;

import com.udacity.midterm.customerReviewsApi.entity.sql.Product;
import com.udacity.midterm.customerReviewsApi.repository.ProductRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Spring REST controller for working with product entity.
 */
@RestController
@RequestMapping("/products")
public class ProductsController {

    // TODO: Wire JPA repositories here
    ProductRepository productRepository;

    public ProductsController (ProductRepository productRepository){
        this.productRepository = productRepository;
    }

    /**
     * Creates a product.
     *
     * 1. Accept product as argument. Use {@link RequestBody} annotation.
     * 2. Save product.
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void createProduct(@Valid @RequestBody Product product) {
        productRepository.save(product);
    }

    /**
     * Finds a product by id.
     *
     * @param id The id of the product.
     * @return The product if found, or a 404 not found.
     */
    @RequestMapping(value = "/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Integer id) {
        Optional<Product> product = productRepository.findById(id);

        return product
                .map(value->new ResponseEntity<>(value, HttpStatus.CREATED))
                .orElseGet(()-> new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }

    /**
     * Lists all products.
     *
     * @return The list of products.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> listProducts() {
        List<Product> productsList = productRepository.findAll();
        return (!productsList.isEmpty()) ? new ResponseEntity<>(productsList, HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}