package com.example.gusonano.chatroom.model;

import com.alibaba.fastjson.annotation.JSONField;

public class Message {

    @JSONField(name = "username")
    private String username;

    @JSONField(name = "msg")
    private String message;

    @JSONField(name = "onlineCount")
    private int onlineCount = 0;

    @JSONField(name = "type")
    private String type = "SPEAK";

    public Message(){}

    public Message(String username, String message, int onlineCount, String type){
        this.username = username;
        this.message = message;
        this.onlineCount = onlineCount;
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getOnlineCount() {
        return onlineCount;
    }

    public void setOnlineCount(int onlineCount) {
        this.onlineCount = onlineCount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
