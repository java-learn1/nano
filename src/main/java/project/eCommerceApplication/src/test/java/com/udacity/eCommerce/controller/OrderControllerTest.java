package com.udacity.eCommerce.controller;

import com.udacity.eCommerce.model.persistence.Cart;
import com.udacity.eCommerce.model.persistence.Item;
import com.udacity.eCommerce.model.persistence.User;
import com.udacity.eCommerce.model.persistence.UserOrder;
import com.udacity.eCommerce.service.OrderService;
import com.udacity.eCommerce.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderControllerTest {

    @Mock
    private UserService userService;

    @Mock
    private OrderService orderService;

    @InjectMocks
    private OrderController orderController;

    private MockMvc mockMvc;

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.standaloneSetup(orderController).build();
    }

    @Test
    public void testSubmitOrderWithoutCart() throws Exception {
        User user = new User();
        user.setUsername("Sander");
        user.setPassword("notSoSecretPassword");
        when(userService.findUser("Sander")).thenReturn(user);

        when(orderService.save(user.getCart())).thenReturn(null);

        this.mockMvc.perform(post("/api/order/submit/Sander"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSubmitOrderHappy() throws Exception {
        Item item = new Item();
        item.setDescription("Some item");
        item.setName("Pepsi");
        item.setId(2L);
        item.setPrice(new BigDecimal("1.50"));

        User user = new User();
        user.setUsername("Guso");
        user.setPassword("notSoSecretPassword");

        Cart cart = new Cart();
        cart.addItem(item);
        cart.setTotal(new BigDecimal("1.50"));
        cart.setId(1L);
        cart.setUser(user);
        user.setCart(cart);
        when(userService.findUser("Guso")).thenReturn(user);

        UserOrder order = new UserOrder();
        order.setUser(user);
        order.setId(20L);
        order.setTotal(new BigDecimal("100.00"));
        when(orderService.save(any())).thenReturn(order);

        this.mockMvc.perform(post("/api/order/submit/Guso"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testSubmitOrderWithoutUser() throws Exception {
        when(userService.findUser("Guso")).thenReturn(null);

        this.mockMvc.perform(post("/api/order/submit/Guso"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void getOrdersForUserHappy() throws Exception {
        Item item = new Item();
        item.setDescription("Some item");
        item.setName("Best item");
        item.setId(12L);
        item.setPrice(new BigDecimal("150.50"));

        User user = new User();
        user.setUsername("Guso");
        user.setPassword("notSoSecretPassword");

        Cart cart = new Cart();
        cart.addItem(item);
        cart.setTotal(new BigDecimal("150.50"));
        cart.setId(1L);
        cart.setUser(user);
        user.setCart(cart);
        when(userService.findUser("Guso")).thenReturn(user);

        UserOrder order = new UserOrder();
        order.setUser(user);
        order.setId(20L);
        order.setTotal(new BigDecimal("100.00"));
        when(orderService.findOrderByUser(any())).thenReturn(Collections.singletonList(order));

        this.mockMvc.perform(get("/api/order/history/Guso"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getOrdersForUserNoOrderFound() throws Exception {
        Item item = new Item();
        item.setDescription("Some item");
        item.setName("Best item");
        item.setId(12L);
        item.setPrice(new BigDecimal("150.50"));

        User user = new User();
        user.setUsername("Guso");
        user.setPassword("notSoSecretPassword");

        Cart cart = new Cart();
        cart.addItem(item);
        cart.setTotal(new BigDecimal("150.50"));
        cart.setId(1L);
        cart.setUser(user);
        user.setCart(cart);
        when(userService.findUser("Guso")).thenReturn(user);


        when(orderService.findOrderByUser(any())).thenReturn(null);

        this.mockMvc.perform(get("/api/order/history/Guso"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void getOrdersForUserNoUserFound() throws Exception {
        when(userService.findUser("Sander")).thenReturn(null);

        this.mockMvc.perform(get("/api/order/history/Sander"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}
