package com.udacity.eCommerce.controller;

import com.udacity.eCommerce.TestUtils;
import com.udacity.eCommerce.model.persistence.User;
import com.udacity.eCommerce.model.persistence.repositories.CartRepository;
import com.udacity.eCommerce.model.persistence.repositories.UserRepository;
import com.udacity.eCommerce.model.requests.CreateUserRequest;

import com.udacity.eCommerce.service.UserService;
import com.udacity.eCommerce.service.UserServiceImp;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.modelmapper.ModelMapper;

public class UserControllerTest {

    private UserController userController;

    private UserService userServiceImp;

    private UserRepository userRepo = mock(UserRepository.class);

    private CartRepository cartRepo = mock(CartRepository.class);

    private BCryptPasswordEncoder encoder = mock(BCryptPasswordEncoder.class);

    @Before
    public void setup(){
        userController = new UserController();
        userServiceImp = new UserServiceImp();
        TestUtils.injectObjects(userController, "userService", userServiceImp);
        TestUtils.injectObjects(userServiceImp, "userRepository", userRepo);
        TestUtils.injectObjects(userServiceImp, "cartRepository", cartRepo);
        TestUtils.injectObjects(userServiceImp, "bCryptPasswordEncoder", encoder);
    }

    @Test
    public void createUserHappyPath() throws Exception{
        when(userServiceImp.generatePassword ("testPassword")).thenReturn("thisHashed");
        CreateUserRequest r = new CreateUserRequest();
        r.setUsername("test");
        r.setPassword("testPassword");
        r.setConfirmPassword("testPassword");

        final ResponseEntity<User> response = userController.createUser(r);
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        User u = response.getBody();
        assertNotNull(u);
        assertEquals(0, u.getId());
        assertEquals("test", u.getUsername());
        assertEquals("thisHashed", u.getPassword());

    }
}
