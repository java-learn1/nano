package com.udacity.eCommerce.controller;

import com.udacity.eCommerce.model.persistence.Item;
import com.udacity.eCommerce.service.ItemServiceImp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ItemControllerTest {
    @InjectMocks
    private ItemController itemController;

    @Mock
    private ItemServiceImp itemService;

    private MockMvc mockMvc;

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.standaloneSetup(itemController).build();
    }

    @Test
    public void getItemsNoItemsFoundEmptyList() throws Exception {
        when(itemService.findAllItems()).thenReturn(Collections.emptyList());

        this.mockMvc.perform(get("/api/item"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void getItemsNoItemsFoundNull() throws  Exception {
        when(itemService.findAllItems()).thenReturn(null);

        this.mockMvc.perform(get("/api/item"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void getItemsHappy() throws Exception {
        Item item = new Item();
        item.setId(0L);
        item.setPrice(new BigDecimal(10));
        item.setName("Pepsi");
        item.setDescription("It will break in no-time");

        when(itemService.findAllItems()).thenReturn(Collections.singletonList(item));

        this.mockMvc.perform(get("/api/item"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getItemByIdHappy() throws Exception {
        Long itemId = 0L;
        when(itemService.findItemById(itemId)).thenReturn(Optional.of(new Item()));

        this.mockMvc.perform(get("/api/item/" + itemId))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getItemByIdNoItemFound() throws Exception {
        Long itemId = 0L;
        when(itemService.findItemById(itemId)).thenReturn(Optional.empty());

        this.mockMvc.perform(get("/api/item/" + itemId))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void getItemsByNameHappy() throws Exception {
        when(itemService.findItemsByName("Pepsi")).thenReturn(Collections.singletonList(new Item()));

        this.mockMvc.perform(get("/api/item/name/{name}", "Pepsi"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getItemsByNameNoItemFound() throws Exception {
        when(itemService.findItemsByName("Udacity")).thenReturn(Collections.singletonList(new Item()));

        this.mockMvc.perform(get("/api/item/name/{name}", "Sander"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}
