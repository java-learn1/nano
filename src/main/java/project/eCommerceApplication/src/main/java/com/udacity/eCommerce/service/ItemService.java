package com.udacity.eCommerce.service;

import com.udacity.eCommerce.model.persistence.Item;

import java.util.List;
import java.util.Optional;

public interface ItemService {
    Optional<Item> findItemById(Long id);

    List<Item> findItemsByName(String item);

    List<Item> findAllItems();


}
