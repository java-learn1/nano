package com.udacity.eCommerce.service;

import com.splunk.logging.SplunkCimLogEvent;
import com.udacity.eCommerce.controller.UserController;
import com.udacity.eCommerce.model.persistence.Cart;
import com.udacity.eCommerce.model.persistence.User;
import com.udacity.eCommerce.model.persistence.repositories.CartRepository;
import com.udacity.eCommerce.model.persistence.repositories.UserRepository;
import com.udacity.eCommerce.model.requests.CreateUserRequest;
import com.udacity.eCommerce.model.splunk.EventLog;
import com.udacity.eCommerce.service.exception.UserInvalidPassword;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImp implements UserService{
    private static final Logger log = LoggerFactory.getLogger(UserServiceImp.class);

    private ModelMapper mapper = new ModelMapper();

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User findUser(String username) {
        return null;
    }

    @Override
    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public User save(CreateUserRequest createUserRequest) {
        validatePassword(createUserRequest.getUsername(), createUserRequest.getPassword(), createUserRequest.getConfirmPassword());

        User user = mapper.map(createUserRequest, User.class);
        log.info("User name set with {}", createUserRequest.getUsername());

        Cart cart = new Cart();
        cartRepository.save(cart);
        user.setCart(cart);

        user.setPassword(generatePassword(createUserRequest.getPassword()));
        userRepository.save(user);

        EventLog eventLog = new EventLog( "user_create_success", "user", "success", "Successfully created user " + createUserRequest.getUsername(), createUserRequest.getUsername(), log);
        return user;
    }

    private void validatePassword(String userName, String password, String confirmPassword) throws UserInvalidPassword {
        if(password.length()<7 ||
                !password.equals(confirmPassword)){
            EventLog eventLog = new EventLog( "validate_password_failed", "user", "error", "Error with user password. Cannot create user " + userName, userName, log);
            throw new UserInvalidPassword("Error with user password. Cannot create user "+userName) ;
        }
    }

    @Override
    public String generatePassword(String password) {
        return bCryptPasswordEncoder.encode(password);
    }
}
