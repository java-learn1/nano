package com.udacity.eCommerce.service;

import com.udacity.eCommerce.model.persistence.Cart;
import com.udacity.eCommerce.model.persistence.User;
import com.udacity.eCommerce.model.persistence.UserOrder;

import java.util.List;

public interface OrderService {
    UserOrder  save (Cart cart);

    List<UserOrder> findOrderByUser(User user);
}
