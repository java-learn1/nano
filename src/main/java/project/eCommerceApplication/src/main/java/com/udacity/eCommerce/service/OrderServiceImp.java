package com.udacity.eCommerce.service;

import com.udacity.eCommerce.model.persistence.Cart;
import com.udacity.eCommerce.model.persistence.User;
import com.udacity.eCommerce.model.persistence.UserOrder;
import com.udacity.eCommerce.model.persistence.repositories.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImp implements OrderService{
    private static final Logger log = LoggerFactory.getLogger(OrderServiceImp.class);

    @Autowired
    OrderRepository orderRepository;

    @Override
    public UserOrder save(Cart cart) {
        UserOrder order = UserOrder.createFromCart(cart);
        if (order == null) {
            log.info("Cannot submit order because username {} does not have a cart", cart.getUser().getUsername());
            return order;
        }
        orderRepository.save(order);
        return order;
    }

    @Override
    public List<UserOrder> findOrderByUser(User user) {
        return orderRepository.findByUser(user);
    }
}
