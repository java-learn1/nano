package com.udacity.eCommerce.service;

import com.udacity.eCommerce.model.persistence.User;
import com.udacity.eCommerce.model.requests.CreateUserRequest;

import java.util.Optional;

public interface UserService {
    User findUser(String username);

    Optional<User> findById(Long id);

    User save(CreateUserRequest createUserRequest);

    String generatePassword(String password);
}
