package com.udacity.eCommerce.controller;

import java.util.List;
import java.util.Optional;

import com.udacity.eCommerce.model.persistence.Item;
import com.udacity.eCommerce.model.persistence.repositories.ItemRepository;
import com.udacity.eCommerce.model.splunk.EventLog;
import com.udacity.eCommerce.service.ItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/item")
public class ItemController {
	Logger log = LoggerFactory.getLogger(ItemController.class);

	@Autowired
	private ItemService itemService;
	
	@GetMapping
	public ResponseEntity<List<Item>> getItems() {
		List<Item> items = itemService.findAllItems();

		if (CollectionUtils.isEmpty(items)) {
			EventLog eventLog = new EventLog( "get_items_empty_list", "get items", "error", "No Items found", "none", log);
			return ResponseEntity.notFound().build();
		}

		EventLog eventLog = new EventLog( "get_items_success", "get items", "success", "Items are returned", "none", log);
		return ResponseEntity.ok(items);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Item> getItemById(@PathVariable Long id) {
		Optional<Item> optionalItem = itemService.findItemById(id);
		if (!optionalItem.isPresent()) {
			EventLog eventLog = new EventLog( "get_item_by_id_not_found", "get item by id", "error", "Item with id " + id + " not found", "none", log);
			ResponseEntity.notFound().build();
		}

		EventLog eventLog = new EventLog( "get_item_by_id_success", "get item by id", "success", "Item id " + id + " was returned", "none", log);
		return ResponseEntity.of(optionalItem);
	}
	
	@GetMapping("/name/{name}")
	public ResponseEntity<List<Item>> getItemsByName(@PathVariable String name) {
		List<Item> items = itemService.findItemsByName(name);
		if(CollectionUtils.isEmpty(items)){
			EventLog eventLog = new EventLog( "get_item_by_name_not_found", "get item by name", "error", "No items for name " + name + " were found in the database", "none", log);
			return ResponseEntity.notFound().build();
		}

		EventLog eventLog = new EventLog( "get_item_by_name_success", "get item by name", "success", "Item " + name + " was returned", "none", log);
		return ResponseEntity.ok(items);
	}
	
}
