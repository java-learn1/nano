package com.udacity.eCommerce.controller;

import java.util.List;

import com.udacity.eCommerce.model.persistence.User;
import com.udacity.eCommerce.model.persistence.UserOrder;
import com.udacity.eCommerce.model.persistence.repositories.OrderRepository;
import com.udacity.eCommerce.model.persistence.repositories.UserRepository;
import com.udacity.eCommerce.model.splunk.EventLog;
import com.udacity.eCommerce.service.OrderService;
import com.udacity.eCommerce.service.OrderServiceImp;
import com.udacity.eCommerce.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/order")
public class OrderController {
	private static final Logger log = LoggerFactory.getLogger(OrderController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private OrderService orderService;
	
	@PostMapping("/submit/{username}")
	public ResponseEntity<UserOrder> submit(@PathVariable String username) {
		User user = userService.findUser(username);
		if(user == null) {
			EventLog eventLog = new EventLog( "no_user_found", "order", "error", "Cannot submit order because username " + username + " was not found in the database", username, log);
			return ResponseEntity.notFound().build();
		}

		UserOrder order = orderService.save(user.getCart());
		if (order == null) {
			EventLog eventLog = new EventLog( "no_order_found", "order", "error", "Cannot submit order because username " + username + " does not have a cart", username, log);
			return ResponseEntity.notFound().build();
		}

		EventLog eventLog = new EventLog( "order_submit_success", "order", "success", "Successfully saved order for username " + username, username, log);
		return ResponseEntity.ok(order);
	}
	
	@GetMapping("/history/{username}")
	public ResponseEntity<List<UserOrder>> getOrdersForUser(@PathVariable String username) {
		User user = userService.findUser(username);
		if(user == null) {
			EventLog eventLog = new EventLog( "history_no_user_found", "order", "error", "Cannot see order history of username " + username + ", because this username was not found in the database", username, log);
			return ResponseEntity.notFound().build();
		}

		List<UserOrder> ordersUser = orderService.findOrderByUser(user);
		if (CollectionUtils.isEmpty(ordersUser)) {
			EventLog eventLog = new EventLog( "empty_order_list", "order", "error", "Cannot see order history of username " + username + ", because this username doesn't have any orders", username, log);
			return ResponseEntity.notFound().build();
		}

		EventLog eventLog = new EventLog( "success", "order", "success", "Successfully returned order history of username " + username, username, log);
		return ResponseEntity.ok(ordersUser);
	}
}
