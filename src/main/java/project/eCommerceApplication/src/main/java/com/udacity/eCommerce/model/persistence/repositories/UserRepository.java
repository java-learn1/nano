package com.udacity.eCommerce.model.persistence.repositories;

import com.udacity.eCommerce.model.persistence.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);
}
