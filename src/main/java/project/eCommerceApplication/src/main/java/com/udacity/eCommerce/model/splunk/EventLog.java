package com.udacity.eCommerce.model.splunk;

import com.splunk.logging.SplunkCimLogEvent;
import org.slf4j.Logger;

public class EventLog {
    Logger log;
    private SplunkCimLogEvent splunkCimLogEvent;
    private String id;
    private String name;
    private String type;
    private String message;
    private String user;

    public EventLog(){

    }

    public EventLog(String id, String name, String type, String message, String user, Logger log) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.message = message;
        this.user = user;
        this.log = log;
        eventToSplunk();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }


    public void eventToSplunk(){
        splunkCimLogEvent = new SplunkCimLogEvent(this.name, this.id);
        splunkCimLogEvent.addField("type", this.type);
        splunkCimLogEvent.addField("message", this.message);
        splunkCimLogEvent.addField("user", this.user);

        eventToLog(splunkCimLogEvent.toString());
    }

    public void eventToLog(String eventLog){
        log.info(eventLog);
    }
}
