package com.udacity.eCommerce.service;

import com.udacity.eCommerce.model.persistence.Item;
import com.udacity.eCommerce.model.persistence.repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ItemServiceImp implements ItemService{

    @Autowired
    private ItemRepository itemRepository;

    @Override
    public Optional<Item> findItemById(Long id) {
        return itemRepository.findById(id);
    }

    @Override
    public List<Item> findItemsByName(String name) {
        return itemRepository.findByName(name);
    }

    @Override
    public List<Item> findAllItems() {
        return itemRepository.findAll();
    }
}
