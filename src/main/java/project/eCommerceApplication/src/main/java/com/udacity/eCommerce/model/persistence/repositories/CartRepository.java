package com.udacity.eCommerce.model.persistence.repositories;

import com.udacity.eCommerce.model.persistence.Cart;
import com.udacity.eCommerce.model.persistence.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<Cart, Long> {
	Cart findByUser(User user);
}
