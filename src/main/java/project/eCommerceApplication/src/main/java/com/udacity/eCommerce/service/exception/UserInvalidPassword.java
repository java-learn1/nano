package com.udacity.eCommerce.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT, reason = "User invalid Password")
public class UserInvalidPassword extends RuntimeException {
    public UserInvalidPassword() {
    }

    public UserInvalidPassword(String message) {
        super(message);
    }
}
