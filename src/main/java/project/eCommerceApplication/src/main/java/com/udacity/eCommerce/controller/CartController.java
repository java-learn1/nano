package com.udacity.eCommerce.controller;

import java.util.Optional;
import java.util.stream.IntStream;

import com.splunk.logging.SplunkCimLogEvent;
import com.udacity.eCommerce.model.persistence.Cart;
import com.udacity.eCommerce.model.persistence.Item;
import com.udacity.eCommerce.model.persistence.User;
import com.udacity.eCommerce.model.persistence.repositories.CartRepository;
import com.udacity.eCommerce.model.persistence.repositories.ItemRepository;
import com.udacity.eCommerce.model.persistence.repositories.UserRepository;
import com.udacity.eCommerce.model.requests.ModifyCartRequest;
import com.udacity.eCommerce.model.splunk.EventLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/api/cart")
public class CartController {
	Logger log = LoggerFactory.getLogger(CartController.class);

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private CartRepository cartRepository;
	
	@Autowired
	private ItemRepository itemRepository;
	
	@PostMapping("/addToCart")
	public ResponseEntity<Cart> addTocart(@RequestBody ModifyCartRequest request) {
		User user = userRepository.findByUsername(request.getUsername());
		if(user == null) {
			EventLog eventLog = new EventLog( "add_to_cart_user_not_found", "add to cart", "error", "Cannot add item to cart because user with username " + request.getUsername() + " was not found in the database", request.getUsername(), log);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		Optional<Item> item = itemRepository.findById(request.getItemId());
		if(!item.isPresent()) {
			EventLog eventLog = new EventLog( "add_to_cart_user_not_found", "add to cart", "error", "Cannot add item to card because no items were found for item with id: " + request.getItemId(), request.getUsername(), log);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		Cart cart = user.getCart();
		IntStream.range(0, request.getQuantity())
			.forEach(i -> cart.addItem(item.get()));
		cartRepository.save(cart);
		EventLog eventLog = new EventLog( "add_to_cart_sucess", "add to cart", "success", "Item with id " + request.getItemId() + " is added to the cart.", request.getUsername(), log);
		return ResponseEntity.ok(cart);
	}
	
	@PostMapping("/removeFromCart")
	public ResponseEntity<Cart> removeFromcart(@RequestBody ModifyCartRequest request) {
		User user = userRepository.findByUsername(request.getUsername());
		if(user == null) {
			EventLog eventLog = new EventLog( "remove_from_cart_user_not_found", "remove from cart", "error", "Cannot add item to cart because user with username " + request.getUsername() + " was not found in the database", request.getUsername(), log);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		Optional<Item> item = itemRepository.findById(request.getItemId());
		if(!item.isPresent()) {
			EventLog eventLog = new EventLog( "remove_from_cart_items_not_found", "remove from cart", "error", "Cannot add item to card because no items were found for item with id: " + request.getItemId(), request.getUsername(), log);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		Cart cart = user.getCart();
		IntStream.range(0, request.getQuantity())
			.forEach(i -> cart.removeItem(item.get()));
		cartRepository.save(cart);
		EventLog eventLog = new EventLog( "remove_from_cart_sucess", "remove from cart", "success", "Item with id " + request.getItemId() + " is removed from the cart.", request.getUsername(), log);

		return ResponseEntity.ok(cart);
	}
		
}
