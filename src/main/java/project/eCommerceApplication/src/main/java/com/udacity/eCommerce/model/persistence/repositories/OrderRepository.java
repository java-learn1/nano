package com.udacity.eCommerce.model.persistence.repositories;

import java.util.List;

import com.udacity.eCommerce.model.persistence.User;
import com.udacity.eCommerce.model.persistence.UserOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<UserOrder, Long> {
	List<UserOrder> findByUser(User user);
}
