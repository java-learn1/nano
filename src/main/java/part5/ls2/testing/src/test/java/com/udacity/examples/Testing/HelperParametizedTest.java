package com.udacity.examples.Testing;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertNotEquals;

@RunWith(Parameterized.class)
public class HelperParametizedTest {
    private String input;
    private String output;

    public HelperParametizedTest(String input, String ouput) {
        super();
        this.input = input;
        this.output = ouput;
    }

    @Parameters
    public static Collection initData(){
        String[][] empNames = {{"Guso", "Sebastían"},{"Guso", "Sebastían"}};
        return Arrays.asList(empNames);
    }

    @Test
    public void verifyNames(){
        assertNotEquals(input,output);
    }

}
