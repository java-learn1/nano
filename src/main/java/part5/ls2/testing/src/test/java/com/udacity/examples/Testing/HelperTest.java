package com.udacity.examples.Testing;

import org.junit.*;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class HelperTest {
    /**
     * this should be public static
     */
    @BeforeClass
    public static void initClass() {
        System.out.println("init Class executed");

    }

    @AfterClass
    public static void teardownclass() {
        System.out.println("teardown Class executed");
    }

    @Before
    public void init() {
        System.out.println("init executed");

    }

    @After
    public void teardown() {
        System.out.println("teardown executed");
    }

    @Test
    public void Test(){
        assertEquals(3,3);
    }

    @Test
    public void verifyGetCount(){
        List<String> empNames = Arrays.asList("Guso", "","Sebastían", "", "");
        assertEquals(2, Helper.getCount(empNames));
    }

    @Test
    public void verifyGetStats(){
        List<Integer> yrsOfExperience = Arrays.asList(13,4,15,6,17,8,19,1,2,3);
        assertEquals(19, Helper.getStats(yrsOfExperience).getMax());
    }

    @Test
    public void verifyGetStringsOfLength3(){
        List<String> empNames = Arrays.asList("Guso", "","Sebastían", "Ana", "Ada");
        assertEquals(2, Helper.getStringsOfLength3(empNames));
    }

    @Test
    public void verifyListIsSquared(){
        List<Integer> yrsOfExperience = Arrays.asList(13,4,15,6,17,8,19,1,2,3);
        List<Integer> expected = Arrays.asList(169, 16, 225, 36, 289, 64, 361, 1, 4, 9);
        assertEquals(expected, Helper.getSquareList(yrsOfExperience));
    }

    @Test
    public void verifyGetMergedList(){
        List<String> empNames = Arrays.asList("Guso", "", "Sebastían","");
        assertEquals("Guso, Sebastían", Helper.getMergedList(empNames));
    }

    @Test
    public void verifyArrayListTest(){
        int[] yrsOfExperience = {13,4,15,6,17,8,19,1,2,3};
        int[] expected = {13,4,15,6,17,8,19,1,2,3};
        assertArrayEquals(expected, yrsOfExperience);
    }

    @Test
    public void verifyGetFilteredList(){
        List<String> empNames = Arrays.asList("Guso", "","Sebastían", "", "");
        List<String> expected = Arrays.asList("Guso","Sebastían");
        assertEquals(expected, Helper.getFilteredList(empNames));
    }
}
