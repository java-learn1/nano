package com.udacity.jdbc;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationVersion;

import java.sql.*;

public class JdbcApplication {

	public static void main(String[] args) throws ClassNotFoundException {
		// STEP 1: Create the JDBC URL for JDND-C3 database
		String url = "jdbc:mysql://localhost:3306/JDND-C3?user=root&password=guso008";

		try{
			// STEP 2: Setup and Run Flyway migration that creates the member table using its Java API
			// https://flywaydb.org/getstarted/firststeps/api#integrating-flyway
			// Note the above link talks about connecting to H2 database, for this exercise, MySQL is used. Adapt the code accordingly.
			// Create the Flyway instance and point it to the database
			Flyway flyway = Flyway.configure().dataSource("jdbc:mysql://localhost:3306/JDND-C3", "root", "guso008")
					.baselineVersion("0").baselineOnMigrate(true).load();

			// Start the migration
			flyway.migrate();

			// STEP 3: Obtain a connection to the JDND-C3 database
			Connection conn = DriverManager.getConnection(url);
			System.out.println("Connected to " + conn.getMetaData().getDatabaseProductName());

			try(Statement stmt = conn.createStatement()){
				// STEP 4: Use Statement to INSERT 2 records into the member table
				// NOTE: The member table is created using Flyway by placing the migration file in src/main/resources/db/migration
				stmt.executeUpdate("INSERT INTO menber VALUES (1, 'Guso 008')");
				stmt.executeUpdate("INSERT INTO menber VALUES (2, 'Iquo')");

				// STEP 5: Read ALL the rows from the member table and print them here
				try(ResultSet rs = stmt.executeQuery("select id, name from menber");){
					while(rs.next()){
						int id = rs.getInt("id");
						String name = rs.getString("name");

						// STEP 6: verify that all inserted rows have been printed
						System.out.println("Id: " + id);
						System.out.println("Name: " + name);
					}
				}
			}

		}catch (SQLException ex){
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}
}
