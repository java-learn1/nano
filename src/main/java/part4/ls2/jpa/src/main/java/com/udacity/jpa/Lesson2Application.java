package com.udacity.jpa;

import com.udacity.jpa.entity.Order;
import com.udacity.jpa.entity.OrderItem;
import com.udacity.jpa.repository.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

@SpringBootApplication
@EnableJpaRepositories
public class Lesson2Application {

	private static final Logger log = LoggerFactory.getLogger(Lesson2Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Lesson2Application.class, args);
	}

	@Bean
	public CommandLineRunner demo(OrderRepository repository) {
		return (args) -> {
			// save a couple of customers
			// STEP 4: Create an order (entity is in Transient state)
			Order order = new Order();
			order.setCustomerName("Guso Olivares");
			order.setCustomerAddress("123 Moctezuma, Guaymallén, Mendoza. AR, 5521");
			order.setCreatedTime(Timestamp.valueOf(LocalDateTime.now()));

			// create order item1
			OrderItem orderItem1 = new OrderItem();
			orderItem1.setItemCount(1);
			orderItem1.setItemName("Alfajores Jorgito");
			orderItem1.setOrder(order);

			// create order item2
			OrderItem orderItem2 = new OrderItem();
			orderItem2.setItemCount(2);
			orderItem2.setItemName("Coca Cola");
			orderItem2.setOrder(order);

			order.setOrderItems(Arrays.asList(orderItem1, orderItem2));

			order = repository.save(order);

			System.err.println("Order ID: " +order.getOrderId());

			Optional<Order> orderRead = repository.findByCustomerName("Guso Olivares");
			orderRead.ifPresent(value->System.err.println("Order: " +value));
		};
	}
}
