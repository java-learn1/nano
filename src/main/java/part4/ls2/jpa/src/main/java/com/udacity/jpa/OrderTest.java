package com.udacity.jpa;

import com.udacity.jpa.entity.Order;
import com.udacity.jpa.entity.OrderItem;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OrderTest {

    private static final String PERSISTENCE_UNIT_NAME = "Order";

    private static EntityManagerFactory factory;

    public static void main(String[] args) {
        // STEP 1: Create a factory for the persistence unit
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        // STEP 2: Create an EntityManager
        EntityManager em = factory.createEntityManager();
        // STEP 3: Start a transaction
        em.getTransaction().begin();
        // STEP 4: Create an order (entity is in Transient state)
        Order order = new Order();
        order.setCustomerName("John Doe");
        order.setCustomerAddress("123 Moctezuma, Guaymallén, Mendoza. AR, 5521");
        order.setCreatedTime(Timestamp.valueOf(LocalDateTime.now()));
        // create order item1
        OrderItem orderItem1 = new OrderItem();
        orderItem1.setItemCount(1);
        orderItem1.setItemName("Alfajores Jorgito");
        orderItem1.setOrder(order);
        // create order item2
        OrderItem orderItem2 = new OrderItem();
        orderItem2.setItemCount(2);
        orderItem2.setItemName("Pepsi");
        orderItem2.setOrder(order);

        order.setOrderItems(Arrays.asList(orderItem1, orderItem2));

        // STEP 5: Persist the order entity
        em.persist(order);
        // NOTE: Order Item is NOT persisted here

        // entity is persistent now
        System.err.println("Order ID:" + order.getOrderId());
        em.getTransaction().commit();
        em.close();

        //Example: How to read an entity
        readOrder(order.getOrderId(), factory);
        factory.close();
    }

    private static void readOrder(Integer orderId, EntityManagerFactory factory) {
        // STEP 1: Create an EntityManager
        EntityManager em = factory.createEntityManager();
        // STEP 2: use the find() method to load an order
        Order order = em.find(Order.class, orderId);
        // OrderItem is fetched eagerly by using a JOIN

        System.err.println("Order: "+order);
        em.close();
    }

   private static void deleteOrder(Integer orderId, EntityManagerFactory factory) {
       // STEP 1: Create an EntityManager
       EntityManager em = factory.createEntityManager();

       // STEP 2: use the find() method to load an order
       Order order = new Order();
       order.setOrderId(orderId);
       em.remove(order);

       System.err.println("Order: " + order);

       em.close();
   }
}