package com.udacity.ls4;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.ReplaceOptions;
import org.bson.Document;
import org.bson.types.ObjectId;

public class Application {

    public static void main(String[] args) {

        // STEP 1: Craft the URI to connect to your local MongoDB server
        MongoClientURI uri = new MongoClientURI(
                "");

        // STEP 2: Create a MongoClient
        MongoClient mongoClient = new MongoClient(uri);

        // STEP 3: Select the jdnd-c3 database to work with
        MongoDatabase database = mongoClient.getDatabase("jdnd-c3");

        // Perform all the steps listed in the exercise
        //database.createCollection("membersdriver");
        MongoCollection<Document> members = database.getCollection("members");
        //Create a document
        Document member = new Document()
                .append("first_name", "Juanito")
                .append("last_name", "Juarez")
                .append("gender", "male")
                .append("age", 23);
        //insert a document
        members.insertOne(member);
        //use the _id for the subsequent operations
        ObjectId _id = member.getObjectId("_id");
        //replace the document just inserted to include "gender" field
        //This translates to db.members.update()
        members.replaceOne(new Document("_id", _id), new Document()
                .append("first_name", "Sana")
                .append("last_name", "Khan")
                .append("gender", "female")
                .append("age", 23));
        //update the document to rename a field.
        members.updateOne(new Document("_id", _id),new Document()
                .append("$rename", new Document("gender", "sex")));

        //upsert here
        members.replaceOne(new Document("first_name", "Naranja"), new Document()
                .append("first_name", "Naranja")
                .append("last_name", "Lima")
                .append("gender", "male")
                .append("age", 29),
                new ReplaceOptions().upsert(true));

        // IMPORTANT: Make sure to close the MongoClient at the end so your program exits.
        mongoClient.close();
    }

}