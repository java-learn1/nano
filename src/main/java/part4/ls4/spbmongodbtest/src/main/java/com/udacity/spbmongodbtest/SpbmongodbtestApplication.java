package com.udacity.spbmongodbtest;

import com.udacity.spbmongodbtest.model.Member;
import com.udacity.spbmongodbtest.repository.MemberRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Optional;

@SpringBootApplication
@EnableMongoRepositories
public class SpbmongodbtestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpbmongodbtestApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(MemberRepository memberRepository) {
		return args -> {
			// STEP 1: Define Member and MemberRepository first before changing this class
			Member member = new Member();
			member.setFirstName("Spmongo");
			member.setLastName("Db");
			member.setAge(25);
			member.setGender("male");

			// STEP 3: save the Member record
			memberRepository.save(member);

			// read the Member using memeber last name
			Optional<Member> memberGet = memberRepository.findById("guso008");

			if(memberGet.isPresent()){
				System.out.println("Member: "+memberGet.get());
			}
		};
	}
}
