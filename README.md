# Java Developer NANODEGREE PROGRAM

Curso tomado de Udacity Java Developer [(Ver más)](https://www.udacity.com/course/java-developer-nanodegree--nd035)

## Course 1: Java Basics
Learn the fundamentals of Java while being introduced to a Spring Boot framework and associated integrations
and plugins.

###### LESSON ONE: Introduction to Java

###### LESSON TWO: Spring Boot Servlet,Filter and Listener

###### LESSON THREE: Spring Boot with Thymeleaf and Mybatis

###### LESSON FOUR: Spring Boot Web Socket with Spring Boot tests

## Course 2: Web Services and APIs
Explore the differences between web services, APIs, and microservices. Develop REST and GraphQL APIs,
and learn how to secure, consume, document, and test those APIs and web services.

###### LESSON ONE Web Services & APIs Overview

###### LESSON TWO Develop REST APIs with Spring Boot

###### LESSON THREE Develop GraphQL APIs with Spring Boot

###### LESSON FOUR Develop Microservices with Spring Boot

###### LESSON FIVE Secure API Endpoints with Spring Security

###### LESSON SIX Consume Web Services and APIs

###### LESSON SEVEN Document REST APIs

###### LESSON EIGHT Test REST APIs

## Course 3: Data Stores & Persistence

Learn about different data stores and how to build persistence for Java applications. Work with
relational and non-relational databases, and use Java to read/write and build test cases for MySQL and
MongoDB.

###### LESSON ONE Web Services & APIs Overview

###### LESSON TWO Develop REST APIs with Spring Boot

###### LESSON THREE NoSQL & MongoDB

###### LESSON FOUR MongoDB for Java

## Course 4: Security and DevOps
Learn about Git, version control, and best practices for authorization and authentication. Use Jenkins to build a
CI/CD pipeline to deploy code to production.

###### LESSON ONE Git

###### LESSON TWO Authorization and Authentication

###### LESSON THREE Testing

###### LESSON FOUR Logging and Analytics

###### LESSON FIVE Jenkins and CI/CD